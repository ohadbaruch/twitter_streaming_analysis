import os
import re
import json
import heapq
from datetime import datetime
from urllib.request import urlopen

from .word_dict import ComparingDict

script_dir = os.path.dirname(__file__)
config_relative_file_path = r'\resources\config.json'
result_data_relative_file_path = r'\resources\result.json'
result_data_abs_file_path = script_dir + result_data_relative_file_path

with open(script_dir + config_relative_file_path, 'r') as config_file:
    config = json.load(config_file)
    data_source = urlopen(
        "{0}://{1}/bck".format(
            config["source_stream_server_protocol"],
            config["source_stream_server"]
        )
    )


def get_streaming_data():
    while 1:
        try:
            line = json.loads(data_source.readline().decode('utf-8'))
            if line == "":
                break
            raw_data = {
                'text': line['text'].replace('\n', ' '),
                'user': line['user']['screen_name'],
                'hash_tags': line['entities']['hashtags'],
                'created_at': line['created_at']
            }
            yield raw_data
        except Exception:
            print("Retrying...")


def update_dict(dict, heap, word):
    try:
        dict[word]['count'] += 1
    except KeyError:
        dict[word] = ComparingDict(word)
        heap.append(dict[word])


def process_raw_data():
    words_dict = dict()
    words_heap = []
    users_dict = dict()
    users_heap = []
    hashtags_dict = dict()
    hashtags_heap = []
    last_minute = -1
    tweets_per_second = 0
    for line in get_streaming_data():
        # Counting words
        line_text = line['text']
        for word in re.findall('([^\W_]+[^\s,.]*)', line_text):
            update_dict(words_dict, words_heap, word)

        # Counting users
        tweeting_user = line['user']
        update_dict(users_dict, users_heap, tweeting_user)

        # Counting hashtags
        if line['hash_tags']:
            for hashtag in line['hash_tags']:
                update_dict(hashtags_dict, hashtags_heap, hashtag['text'])

        current_minute = datetime.strptime(line['created_at'], '%a %b %d %H:%M:%S +0000 %Y').minute
        if last_minute == current_minute:
            tweets_per_second += 1
        else:
            last_minute = current_minute
            tweets_per_second = 1

        heapq._heapify_max(words_heap)
        heapq._heapify_max(users_heap)
        heapq._heapify_max(hashtags_heap)
        output_dict = {
            'top_10_words': {},
            'top_10_users': {},
            'top_10_hashtags': {},
            'avg_tweets_per_second': tweets_per_second / 60
        }
        for i in range(10):
            if i < len(words_heap):
                word_dict = words_heap[i]
                output_dict['top_10_words'][word_dict['word']] = word_dict['count']
            if i < len(users_heap):
                user_dict = users_heap[i]
                output_dict['top_10_users'][user_dict['word']] = user_dict['count']
            if i < len(hashtags_heap):
                hashtag_dict = hashtags_heap[i]
                output_dict['top_10_hashtags'][hashtag_dict['word']] = hashtag_dict['count']
        with open(result_data_abs_file_path, 'w', buffering=1) as result_data_file:
            result_data_file.write(json.dumps(output_dict) + '\n')
