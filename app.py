import json
import threading

from flask import Flask
from flask_cors import CORS, cross_origin

from data_analysis.read_stream import (
    get_streaming_data,
    process_raw_data,
    result_data_abs_file_path
)

app = Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'


@app.route('/')
@cross_origin()
def stream_stats():
    try:
        with open(result_data_abs_file_path, 'r') as result_data_file:
            line = result_data_file.readline()
        return json.loads(line)
    except json.decoder.JSONDecodeError:
        return "No data yet..."


if __name__ == '__main__':
    threads = list()
    threads.append(threading.Thread(target=get_streaming_data))
    threads.append(threading.Thread(target=process_raw_data))
    for worker in threads:
        worker.start()
    app.run()
