

class ComparingDict(dict):
    def __init__(self, word):
        super(ComparingDict, self).__init__()
        self['word'] = word
        self['count'] = 1

    def __lt__(self, other):
        return self['count'] < other['count']

    def __gt__(self, other):
        return self['count'] > other['count']
